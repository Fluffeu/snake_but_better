extends Node2D

var states = {}
var currentState = null
var host = null
export var initialState = "None"

func _ready():
	host = get_parent()
	for child in get_children():
		states[child.name] = child
	change_state(initialState)

func change_state(newState):
	if currentState:
		currentState.exit_base()
		currentState.exit(host)
	currentState = states[newState]
	currentState.enter_base()
	currentState.enter(host)

func change_state_if_current(state, newState):
	if currentState == state:
		change_state(newState)

func _process(delta):
	currentState.perform(host, delta)

func _physics_process(delta):
	currentState.physics_perform(host, delta)