extends "State.gd"

export var speed = 350.0
var startingPoint = Vector2(0, 0)
var totalTime = 0.0

func enter(host):
	startingPoint =  global_position
	totalTime = 0.0
	if get_child(0).has_method("randomize_angle"):
		get_child(0).randomize_angle()

func physics_perform(host, delta):
	totalTime += speed*delta
	host.global_position = startingPoint + get_child(0).get_position_in_time(totalTime)