extends Node2D

export var switchTimer = -1
export var nextStateTimer = "None"
export var snakeDistanceSwitch = 0
export var nextStateSnake = "None"
export var baloonDistanceSwitch = 0
export var nextStateBaloon = "None"

var currentSwitchTimer = -1
var isTimerWorking = false

func _ready():
	currentSwitchTimer = switchTimer

func _process(delta):
	if switchTimer != -1 and isTimerWorking:
		currentSwitchTimer -= delta
		if currentSwitchTimer <= 0:
			get_parent().change_state_if_current(self, nextStateTimer)
	if snakeDistanceSwitch != 0:
		if snakeDistanceSwitch > 0 and TreeUtilities.get_fast("Snake").global_position.distance_to(global_position) < snakeDistanceSwitch:
			get_parent().change_state_if_current(self, nextStateSnake)
			#print(TreeUtilities.get_fast("Snake").global_position.distance_to(global_position))
		elif snakeDistanceSwitch < 0 and TreeUtilities.get_fast("Snake").global_position.distance_to(global_position) > -1*snakeDistanceSwitch:
			get_parent().change_state_if_current(self, nextStateSnake)
	if baloonDistanceSwitch != 0:
		if baloonDistanceSwitch > 0 and TreeUtilities.get_fast("baloon").global_position.distance_to(global_position) < baloonDistanceSwitch:
			get_parent().change_state_if_current(self, nextStateBaloon)
		elif baloonDistanceSwitch < 0 and TreeUtilities.get_fast("baloon").global_position.distance_to(global_position) > -1*baloonDistanceSwitch:
			get_parent().change_state_if_current(self, nextStateBaloon)

func enter_base():
	isTimerWorking = true
	currentSwitchTimer = switchTimer

func exit_base():
	isTimerWorking = false

func enter(host):
	pass
	
func exit(host):
	pass
	
func perform(host, delta):
	pass
	
func physics_perform(host, delta):
	pass