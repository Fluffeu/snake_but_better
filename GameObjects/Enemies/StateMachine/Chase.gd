extends "State.gd"

export (String, "Baloon", "Snake") var chaseType = "Snake"
export var speed = 350.0
var chasedObject = null

func _ready():
	chasedObject = TreeUtilities.get_fast(chaseType)

func physics_perform(host, delta):
	host.move_towards(chasedObject.global_position, speed*delta)