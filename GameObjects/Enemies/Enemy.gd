extends KinematicBody2D

func _process(delta):
	if global_position.distance_to(Vector2(1000, 1000)) > 5000:
		queue_free()

func move_towards(point, speed):
	var collidingObject = null
	if global_position.distance_to(point) < speed:
		collidingObject = move_and_collide(point - global_position)
	else:
		collidingObject = move_and_collide((point - global_position).normalized()*speed)
	if collidingObject:
		if collidingObject.collider:
			if collidingObject.collider.has_method("destroyed_by_enemy"):
				collidingObject.collider.destroyed_by_enemy()
				die()
		
func destroyed_by_baloon():
	die()
	var pickup = load("res://GameObjects/Pickups/TestPickup/TestPickup.tscn").instance()
	pickup.global_position = global_position
	TreeUtilities.get_fast("Level").add_child(pickup)

func die():
	queue_free()
#	var particles = load("res://Environment/TestParticles.tscn").instance()
#	particles.global_position = global_position
#	TreeUtilities.get_fast("Level").add_child(particles)
#	particles.emitting = true