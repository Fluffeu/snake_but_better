extends KinematicBody2D

var trajectory = null
var startingPoint = Vector2(0, 0)
var totalTime = 0.0
var shaderTime = 1.0
var maxPos = Vector2(2000, 2000)
var minPos = Vector2(-2000, -2000)
var interactiveTimer = 0.0
export var speed = 200.0
export var colliderRadius = 4.0
export var afterBounceTrajectory = "LinearTrajectory"

func _process(delta):
	shaderTime += delta
	interactiveTimer -= delta
	if global_position.x > maxPos.x or global_position.x < minPos.x or global_position.y > maxPos.y or global_position.y < minPos.y:
		queue_free()
	if shaderTime > 0.5:
		$BounceEffect.get_material().set_shader_param("time", 0)
	else:
		$BounceEffect.get_material().set_shader_param("time", shaderTime)

func _ready():
	startingPoint = global_position
	trajectory = $Trajectories.get_child(0)

func _physics_process(delta):
	totalTime += speed*delta
	if trajectory != null:
		global_position = startingPoint + trajectory.get_position_in_time(totalTime)

func destroy_baloon(baloon):
	baloon.pop() 

func interact_with_tail(begP, endP, collisionPoint):
	if interactiveTimer > 0:
		interactiveTimer = 0.4
		return
	var d = (global_position.x - begP.x)*(endP.y - begP.y) - (global_position.y - begP.y)*(endP.x - begP.x)
	var normal = (endP - begP).normalized()
	if d > 0:
		normal = normal.rotated(-PI/2)
	else:
		normal = normal.rotated(PI/2)
	var preReflectionVec = Vector2(1, 0).rotated(trajectory.angle)
	var reflectionVec = preReflectionVec - 2.0 * (preReflectionVec.dot(normal))*normal
	trajectory.angle = Vector2(1, 0).angle_to(reflectionVec)
	totalTime = 0
	startingPoint = global_position
	shaderTime = 0
	interactiveTimer = 0.4

func change_direction(newDirection):
	trajectory.angle = Vector2(1, 0).angle_to(newDirection)
	totalTime = 0
	startingPoint = global_position
	shaderTime = 0

func set_initial_parameters(params):
	trajectory = $Trajectories.get_child(0)
	if params.size() < 2:
		return
	if trajectory != null and params[0] == "fixed":
		trajectory.angle = params[1]
	elif trajectory != null and params[0] == "rand":
		trajectory.angle = Rands.rand_spawn_fly_angle(global_position)