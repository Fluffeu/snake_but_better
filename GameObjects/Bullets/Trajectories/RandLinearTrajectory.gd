extends "Trajectory.gd"

func get_position_in_time(time):
	return Vector2(1, 0).rotated(angle)*time

func randomize_angle():
	angle = rand_range(0.0, 2.0*PI)
