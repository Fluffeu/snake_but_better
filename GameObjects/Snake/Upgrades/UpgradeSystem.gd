extends Node

var currentExp = 0
var currentLevel = 1

signal snake_level_up
signal exp_changed

func _ready():
	TreeUtilities.get_fast("Snake").connect("experience_gained", self, "on_exp_gained")
	TreeUtilities.get_fast("Snake").connect("damage_taken", self, "on_exp_loss")

func on_exp_gained(amount):
	currentExp += amount
	if currentExp >= exp_to_lvl_up():
		currentExp = 0
		level_up()
		emit_signal("snake_level_up", currentLevel)
	emit_signal("exp_changed", currentExp, exp_to_lvl_up())
	

func exp_to_lvl_up():
	return pow(currentLevel, 0.6)*5

func on_exp_loss():
	var currentExpRatio = currentExp/exp_to_lvl_up()
	level_down()
	currentExp = exp_to_lvl_up()*currentExpRatio
	emit_signal("snake_level_up", currentLevel)
	emit_signal("exp_changed", currentExp, exp_to_lvl_up())

func level_up():
	currentLevel += 1
	if get_child_count() >= currentLevel-1:
		get_child(currentLevel-2).apply()

func level_down():
	if currentLevel > 1:
		if get_child_count() >= currentLevel-1:
			get_child(currentLevel-2).remove()
		currentLevel -= 1
