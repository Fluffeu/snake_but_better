extends "Upgrade.gd"

export var additionalLength = 200.0

func apply():
	TreeUtilities.get_fast("SnakeTail").length += additionalLength

func remove():
	TreeUtilities.get_fast("SnakeTail").length -= additionalLength

