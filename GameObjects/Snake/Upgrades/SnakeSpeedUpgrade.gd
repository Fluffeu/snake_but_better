extends "Upgrade.gd"

export var additionalSpeed = 100.0

func apply():
	TreeUtilities.get_fast("Snake").speed += additionalSpeed

func remove():
	TreeUtilities.get_fast("Snake").speed -= additionalSpeed

