extends KinematicBody2D

export var speed = 300.0
var experience = 0.0

var movementDirection = Vector2(1, 0)

signal damage_taken
signal test_signal
signal experience_gained

func _ready():
	$Sprites.rotation = Vector2(0, 1).angle_to(movementDirection)

func _input(event):
	if Input.is_action_just_pressed("game_point_direction"):
		movementDirection = (get_global_mouse_position() - global_position).normalized()
		$Sprites.rotation = Vector2(0, 1).angle_to(movementDirection)
	if Input.is_action_just_pressed("test"):
		emit_signal("test_signal")

func _physics_process(delta):
	move_and_collide(movementDirection*speed*delta)
	$SnakeTail.add_point(global_position)

func collect_pickup(pickup):
	emit_signal("experience_gained", pickup.expValue)

func destroyed_by_enemy():
	emit_signal("damage_taken")