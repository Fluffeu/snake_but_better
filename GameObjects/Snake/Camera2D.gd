extends Camera2D

export var shakeStrength = 0.0
export var shakeTime = 0.0

func _ready():
	TreeUtilities.get_fast("Snake").connect("damage_taken", self, "shake1")
	TreeUtilities.get_fast("Baloon").connect("damage_taken", self, "shake1")

func shake1():
	shake(10, 1.0)

func shake(strength, time):
	shakeStrength = strength
	shakeTime = time

#some problems after godot 3.1 with shake on fullscreen
func _process(delta):
	if shakeTime > 0:
		offset = Vector2(randf()*shakeStrength, randf()*shakeStrength)
		print(global_position + offset)
	else:
		offset = Vector2(0, 0)
		print(global_position + offset)
	shakeTime -= delta