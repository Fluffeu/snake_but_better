extends Line2D

var currentExp = 0.0
var maxExp = 1.0

func _ready():
	TreeUtilities.get_fast("UpgradeSystem").connect("exp_changed", self, "on_exp_changed")

func on_exp_changed(ce, me):
	currentExp = ce
	maxExp = me
	
func _physics_process(delta):
	global_position = Vector2(0, 0)
	var barLength = currentExp/maxExp*get_parent().length
	var p = get_parent().points
	var endPoints = [p[p.size()-1]]
	var i = p.size() - 2
	while i >= 0 and barLength > 0:
		barLength -= p[i].distance_to(p[i+1])
		if barLength > 0:
			endPoints.append(p[i])
		i -= 1
	
	points = endPoints
