extends Area2D

var points = []

export var length = 200.0

func _input(event):
	if Input.is_action_just_pressed("test"):
		length += 50.0

func _physics_process(delta):
	$MainTailLine.global_position = Vector2(0, 0)
	#add_point(global_position)
	$MainTailLine.points = points
	_check_tail_collisions()

func _process(delta):
	_update_points()

func add_point(newPoint):
	points.append(newPoint)

func _update_points():
	var isDeleting = false
	var pointToAdd = null
	var lengthLeft = length
	var i = points.size() - 2
	while i >= 0:
		if isDeleting:
			points.remove(i)
		elif points[i].distance_to(points[i+1]) > lengthLeft:
			pointToAdd = points[i]+(points[i] - points[i+1]).normalized()*lengthLeft
			isDeleting = true
		else:
			lengthLeft -= points[i].distance_to(points[i+1])
		i -= 1
	if pointToAdd:
		points.push_front(pointToAdd)

func _check_tail_collisions():
	var i = points.size() - 2
	while i >= 0:
		$RayCast2D.global_position = points[i]
		$RayCast2D.cast_to = points[i+1]-points[i]
		$RayCast2D.force_raycast_update()
		if $RayCast2D.is_colliding():
			if $RayCast2D.get_collider():
				if $RayCast2D.get_collider().has_method("interact_with_tail"):
					$RayCast2D.get_collider().interact_with_tail(points[i], points[i+1], $RayCast2D.get_collision_point())
		i -= 1
	for body in get_overlapping_bodies():
		if body.has_method("change_direction"):
			body.change_direction(TreeUtilities.get_fast("Snake").movementDirection)