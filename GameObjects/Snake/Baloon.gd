extends Area2D

var followTarget = null
export var maxDistance = 300
export var speed = 300.0
var lastMovementDirection = Vector2(1, 0)
var deactivationTimer = 0.0
export var deactivationCooldown = 5.0
var isActive = true

signal damage_taken

func _ready():
	followTarget = get_parent().get_node("Snake")

func _physics_process(delta):
	if !isActive:
		return
	
	for body in get_overlapping_bodies():
		if body.has_method("destroyed_by_baloon"):
			body.destroyed_by_baloon()
		if body.has_method("destroy_baloon"):
			body.destroy_baloon(self)
			
	if global_position.distance_to(followTarget.global_position) > maxDistance:
		lastMovementDirection = (followTarget.global_position - global_position).normalized()
		global_position += lastMovementDirection*(global_position.distance_to(followTarget.global_position)-maxDistance)
	else:
		global_position += delta*lastMovementDirection*speed

func _process(delta):
	deactivationTimer -= delta
	if deactivationTimer <= 0.0 and !isActive:
		activate()
	if !isActive:
		return
	$Line2D.global_position = Vector2(0, 0)
	$Line2D.points[0] = global_position
	$Line2D.points[1] = followTarget.global_position
	$Line2D.width = 20/pow(global_position.distance_to(followTarget.global_position)+0.01, 0.5)

func pop():
	emit_signal("damage_taken")
	deactivate()

func deactivate():
	deactivationTimer = deactivationCooldown
	isActive = false
	global_position = Vector2(100000, 100000)
	hide()

func activate():
	isActive = true
	global_position = followTarget.global_position
	lastMovementDirection = followTarget.movementDirection
	show()

