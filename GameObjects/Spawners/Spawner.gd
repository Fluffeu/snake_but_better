extends Node2D

export (String, "Bullet", "Enemy") var objectType = "Enemy"
export var objectName = "TestEnemy"
export var spawnTimer = 5.0
export var spawnParameters = []
var object = null
var currentTimer = 0.0

func _ready():
	if objectType == "Enemy":
		object = load("res://GameObjects/Enemies/" + objectName + "/" + objectName + ".tscn")
	else:
		object = load("res://GameObjects/Bullets/" + objectName + "/" + objectName + ".tscn")
	pass

func _process(delta):
	currentTimer += delta
	if currentTimer >= spawnTimer:
		currentTimer = 0.0
		_spawn()
	pass

func _spawn():
	var newObject = object.instance()
	newObject.global_position = $PointGenerator.random_point()
	TreeUtilities.get_fast("Level").add_child(newObject)
	if newObject.has_method("set_initial_parameters"):
		newObject.set_initial_parameters(spawnParameters)