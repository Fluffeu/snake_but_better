extends "res://GameObjects/Spawners/PointGenerator/PointGenerator.gd"

export var direction = Vector2(1, 0)

func random_point():
	return global_position + randf()*direction
