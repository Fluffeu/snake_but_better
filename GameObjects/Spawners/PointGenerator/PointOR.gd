extends "res://GameObjects/Spawners/PointGenerator/PointGenerator.gd"

func random_point():
	var childNo = rand_range(0, get_child_count())
	if get_child(childNo).has_method("random_point"):
		return get_child(childNo).random_point()
	else:
		print("Invalid child for PointOR.")
		return Vector2(0, 0)
