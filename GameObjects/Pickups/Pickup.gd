extends Area2D

export var expValue = 1.0

func _physics_process(delta):
	for body in get_overlapping_bodies():
		if body.has_method("collect_pickup"):
			body.collect_pickup(self)
			queue_free()
