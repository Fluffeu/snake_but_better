extends Node

var nodesFound = {}

func get_fast(nodeName):
	if !nodesFound.has(nodeName) or nodesFound[nodeName] == null:
		nodesFound[nodeName] = find_node_by_name(nodeName)
	return nodesFound[nodeName]

func find_node_by_name(nodeName):
	print("Searching for node: " + nodeName);
	return _find_node_by_name(nodeName, get_tree().get_root());
	pass

func _find_node_by_name(nodeName, node):
	if node.name == "MarkedForDeletion":
		return null;
	if node.name == nodeName:
		return node;
	for c in node.get_children():
		var result = _find_node_by_name(nodeName, c);
		if result != null:
			return result;
	return null;
	pass

func find_nodes_with_method(method):
	var result = [];
	_find_nodes_with_method(method, get_tree().get_root(), result);
	return result;
	pass 

func _find_nodes_with_method(method, node, result):
	if node.name == "MarkedForDeletion":
		return;
	if node.has_method(method):
		result.append(node);
	for c in node.get_children():
		_find_nodes_with_method(method, c, result);
	pass

func get_resource_path(resource):
	var path = resource.filename
	var last_slash_position = 0
	for i in range(0, path.length()):
		if path[i] == '/':
			last_slash_position = i
	return path.left(last_slash_position)
	pass