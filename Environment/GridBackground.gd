extends Sprite

var referenceObject = null
var lastObjectPosition = Vector2(0.0, 0.0)
var lastDirection = Vector2(1.0, 1.0)
export var shakeAmp = 0.0
export var shakePeriodScale = 500.0
export var shakeTimeScale = 70.0
export var wavesTimeH = 1000.0
export var wavesTimeScaleH = 10.0
export var wavesTimeV = 1000.0
export var wavesTimeScaleV = 10.0

func _ready():
	referenceObject = TreeUtilities.get_fast("Baloon")
	TreeUtilities.get_fast("Snake").connect("damage_taken", self, "shake")
	TreeUtilities.get_fast("Snake").connect("test_signal", self, "waves_calm_random")
	TreeUtilities.get_fast("Baloon").connect("damage_taken", self, "shake")

func shake():
	$AnimationPlayer.play("shake")

func waves_calm_random():
	wavesTimeH = 0.0

func _process(delta):
	var distanceToObject = referenceObject.global_position - global_position
	var size = texture.get_size()
	var uvCoord = Vector2(0.5 + distanceToObject.x/size.x, 0.5+distanceToObject.y/size.y)
	get_material().set_shader_param("gravityCentre", uvCoord)
	if lastObjectPosition.x < referenceObject.global_position.x:
		lastDirection.x = max(-1.0, lastDirection.x - 3.0*delta)
	else:
		lastDirection.x = min(1.0, lastDirection.x + 3.0*delta)
	if lastObjectPosition.y < referenceObject.global_position.y:
		lastDirection.y = max(-1.0, lastDirection.y - 3.0*delta)
	else:
		lastDirection.y = min(1.0, lastDirection.y + 3.0*delta)
	get_material().set_shader_param("gravityDirection", lastDirection)
	lastObjectPosition = referenceObject.global_position
	get_material().set_shader_param("shakeAmplitude", shakeAmp)
	get_material().set_shader_param("periodScale",  shakePeriodScale)
	get_material().set_shader_param("timeScale", shakeTimeScale)
	
	wavesTimeH += delta
	wavesTimeV += delta
	get_material().set_shader_param("newEffectTime", Vector2(wavesTimeH*wavesTimeScaleH, wavesTimeV*wavesTimeScaleV))