extends Sprite

var baloon = null

func _ready():
	baloon = TreeUtilities.get_fast("Baloon")

func _process(delta):
	var distanceToObject = baloon.global_position - global_position
	var size = Vector2(texture.get_size().x*scale.x, texture.get_size().y*scale.y)
	var uvCoord = Vector2(0.5 + distanceToObject.x/size.x, 0.5+distanceToObject.y/size.y)
	get_material().set_shader_param("lightSourcePos", uvCoord)
