shader_type canvas_item;

uniform vec2 gravityCentre = vec2(0.5, 0.5);
uniform float gravityRange = 0.03;
uniform vec2 gravityDirection = vec2(1.0, 1.0);

uniform float shakeAmplitude = 0.0;
uniform float periodScale = 500.0;
uniform float timeScale = 70.0;

uniform vec2 newEffectStartingPoint = vec2(0.5, 0.5);
uniform vec2 newEffectTime = vec2(1000.0, 1000.0);
uniform vec2 newEffectAmp = vec2(0.03, 0.05);
uniform vec2 newEffectPeriodMult = vec2(200.0, 200.0);
uniform vec2 newEffectAntiWidthMult = vec2(50.0, 30.0);
uniform vec2 newEffectTranslation = vec2(0.0, 15.0);
uniform vec2 newEffectDecrSpd = vec2(1.5, 1.5);

void fragment()
{
	bool inRange = (UV.x -gravityCentre.x)*(UV.x -gravityCentre.x)+(UV.y -gravityCentre.y)*(UV.y -gravityCentre.y) < gravityRange*gravityRange;
	float displacementLength = float(inRange)*pow(abs((gravityRange-length(gravityCentre - UV))), 1.3)*pow(length(gravityCentre - UV), 0.0);
	vec2 displacement = gravityDirection*displacementLength;
	
	vec2 shakeDisplacement = vec2(sin(UV.y*periodScale+TIME*timeScale), sin(UV.x*periodScale+TIME*timeScale));
	
	vec2 newEffectDisplacement;
	newEffectDisplacement.y = sin(UV.x*newEffectPeriodMult.x)*newEffectAmp.x*(1.0/3.0/(1.0 + pow(abs(abs((UV.x-newEffectStartingPoint.x)*newEffectAntiWidthMult.x - newEffectTranslation.x) - newEffectTime.x), newEffectDecrSpd.x)));
	newEffectDisplacement.x = sin(UV.y*newEffectPeriodMult.y)*newEffectAmp.y*(1.0/3.0/(1.0 + pow(abs(abs((UV.y-newEffectStartingPoint.y)*newEffectAntiWidthMult.y - newEffectTranslation.y) - newEffectTime.y), newEffectDecrSpd.y)));
	
	vec2 totalDisplacement = displacement + shakeAmplitude*shakeDisplacement + newEffectDisplacement;
	
	bool isOutsideTexture = (UV.x + totalDisplacement.x) < 0.0 || (UV.x + totalDisplacement.x) > 1.0 || (UV.y + totalDisplacement.y) > 1.0 || (UV.y + totalDisplacement.y) < 0.0;
	COLOR.rgba = float(!isOutsideTexture)*texture(TEXTURE, UV+totalDisplacement).rgba;
}