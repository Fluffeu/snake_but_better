extends ViewportContainer

func _ready():
	$Viewport.size = get_parent().rect_size
	get_parent().connect("resized", self, "on_resized")

func on_resized():
	$Viewport.size = get_parent().rect_size
