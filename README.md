This is the game I created, to develope on vfx and shader usage.

How to play:
- use your mouse to controll the snake.
- red enemies can't touch your baloon (red ball)
- blue enemies can't touch your head
- destroy blue enemies using baloon
- reflect red enemies with your tail
- collect yellow balls dropped by blue enemies to level up and become stronger