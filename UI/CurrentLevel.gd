extends Label

func _ready():
	text = "1"
	TreeUtilities.get_fast("UpgradeSystem").connect("snake_level_up", self, "on_lvl_up")

func on_lvl_up(currentLevel):
	text = str(currentLevel)
