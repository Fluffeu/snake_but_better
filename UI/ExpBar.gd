extends ProgressBar

func _ready():
	TreeUtilities.get_fast("UpgradeSystem").connect("exp_changed", self, "on_exp_changed")

func on_exp_changed(currentExp, maxExp):
	value = currentExp
	max_value = maxExp
